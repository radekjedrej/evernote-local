import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import LatestNotes from './components/LatestNotes'
import CreateNotes from './components/CreateNotes'
import Header from './components/Header'
import './styles/styles.scss';

class App extends React.Component {
  render() {
    return (
      <Router>
        <Header />
        <Switch>
            <Route path="/" component={LatestNotes} exact={true}  />
            <Route path="/create" component={CreateNotes} exact={true}  />
        </Switch>
      </Router>
    );
  }
}

export default App;
