import React from 'react'
import firebase from '../firebase';
import NotesList from './NotesList';

class LatestNotes extends React.Component {
    constructor() {
        super();
        this.state = {
          notes: ''
        };
    }

    render() {
        const { notes } = this.state
        
        return (
            <div>
                <h1>Latest Notes</h1>
                <NotesList notes={notes} deleteNote={this.deleteNote}/>
            </div>
        )
    }

    deleteNote = (note) => {
        firebase
        .firestore()
        .collection('notes')
        .doc(note)
        .delete();
    }

    componentDidMount() {
        firebase
          .firestore()
          .collection('notes')
          .onSnapshot(serverUpdate => {
            const notes = serverUpdate.docs.map(_doc => {
              const data = _doc.data();
              data['id'] = _doc.id;
              return data;
            })
            this.setState({ notes })
        })
    }
}

export default LatestNotes