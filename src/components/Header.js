import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => (
    <header className="navigation">
        <NavLink to="/" activeClassName="is-active" exact>Home</NavLink>
        <NavLink to="/create" activeClassName="is-active">Create Note</NavLink>
    </header>
)


export default Header