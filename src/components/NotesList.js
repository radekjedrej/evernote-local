import React from 'react'

const NotesList = ({ notes, deleteNote }) => (
    <div>
        <div>
            { notes ? (
                notes.map(note => {
                    return (
                        <div key={note.id}>
                            <h3>{note.title}</h3>
                            <p>{note.body}</p>
                            <button onClick={() => deleteNote(note.id)}>Delete</button>
                        </div>
                    )
                })
            ) : null}
        </div>
    </div> 
)

export default NotesList;